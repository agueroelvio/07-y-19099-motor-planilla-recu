const Joi = require("joi");
const integranteStoreSchema = Joi.object({
    // van a venir los campos y las reglas de validacion que se aplica en cada campo
    matricula: Joi.string().required().min(5).label("Matricula"),
    nombre: Joi.string().min(3).required().label("Nombre"),
    apellido: Joi.string().min(3).required().label("Apellido"),
    rol: Joi.string().valid('desarrollador', 'ayudante', 'analista').required().label("Rol"),
    codigo: Joi.string().required().label("Codigo"),
    url: Joi.string().optional().label("Url"),
    esta_borrado: Joi.required().label("Estado")

 });

 module.exports = integranteStoreSchema;



