const express = require('express');
const { getALL, db } = require('../../db/conexion'); 
const multer = require('multer');
const upload = multer({ dest: '../../public/assets' });
const fileUpload = upload.single('imagen');
const integranteStoreSchema = require('../../validators/integrantes/createIntegrantes');

const IntegranteController = {
    index: async function (req, res) {
        try {

            let sql = "SELECT * FROM integrante WHERE esta_borrado = 0";
            console.log("Filtros de busqueda", req.query);
            for (const prop in req.query['s']){
                if (req.query['s'][prop]) { // solo para valores de busqueda del formulario
                console.log("prop", prop, req.query['s'][prop]);
                sql += ` AND ${prop} = '${req.query['s'][prop]}'`;
                }
            }
            console.log('sql query', sql);
            const integrantes = await getALL(sql);

            if (integrantes.length === 0) {
                return res.render("admin/Integrante/index", { message: "No hay integrantes", integrantes: [] });
            }

            const processedIntegrantes = await Promise.all(integrantes.map(async integrante => {
                const datos = await getALL(`SELECT * FROM Media WHERE matricula = '${integrante.matricula}'`);
                return {
                    ...integrante,
                    esta_borrado: integrante.esta_borrado ? 'Inactivo' : 'Activo',
                    datos
                };
            }));

            const message = req.query.message || null;
            res.render("admin/Integrante/index", { integrantes: processedIntegrantes, message });
        } catch (error) {
            console.error(error);
            return res.status(500).render("error");
        }
    },
    create: async function (req, res) {
        const message = req.query.message;
        const datosForm = req.query;
        res.render('admin/Integrante/crearForm', {
            message: message,
            datosForm: datosForm,
        });
    },
    store: async function (req, res) {
        try {
            const { matricula, nombre, apellido, rol, codigo, url, esta_borrado } = req.body;
            const normalizedRol = rol ? rol.toLowerCase() : '';

            const {error, value} = integranteStoreSchema.validate(
                req.body
            ); 
            if(err){
                console.error("Error al insertar en la base de datos:", err);
                return res.sendStatus(500);
            }
            
            
                if (err) {
                console.error("Error al insertar en la base de datos:", err.details.joi);
                return res.sendStatus(500);
            }
            res.redirect("/admin/Integrante/crear?message=Integrante creado exitosamente");
        
    
    
            

            if (normalizedRol && normalizedRol !== 'desarrollador' && normalizedRol !== 'ayudante') {
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/Integrante/crear?message=El rol debe ser "Desarrollador" o "Ayudante"&${queryParams}`);
            }

            const maxOrdenResult = await new Promise((resolve, reject) => {
                db.get("SELECT MAX(orden) as maxOrden FROM Integrante", (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row);
                });
            });

            const newOrden = (maxOrdenResult.maxOrden || 0) + 1;
            db.run(
                "INSERT INTO Integrante (matricula, nombre, apellido, rol, codigo, url, esta_borrado, orden) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                [matricula, nombre, apellido, rol, codigo, url, esta_borrado, newOrden],
                (err) => {
                    if (error) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/Integrante/crear?message=Integrante creado exitosamente");
                }
            );
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.status(500).send('Error al insertar en la base de datos');
        }
    },
    update: function (req, res) {
        const idIntegrante = req.params.idIntegrante;
        const { rol, codigo, url, esta_borrado } = req.body;

        db.run(
            "UPDATE Integrante SET rol = ?, codigo = ?, url = ?, esta_borrado = ? WHERE id = ?",
            [rol, codigo, url, esta_borrado, idIntegrante],
            function (err) {
                if (err) {
                    console.error("Error al actualizar el integrante:", err);
                    return res.redirect(`/admin/Integrante/edit/${idIntegrante}?message=Error al actualizar el integrante`);
                }
                console.log(`Integrante con ID ${idIntegrante} actualizado.`);
                res.redirect('/admin/Integrante/listar?message=Integrante actualizado exitosamente');
            }
        );
    },
    edit: function (req, res) {
        const idIntegrante = req.params.idIntegrante;
        console.log("ID del Integrante:", idIntegrante);

        db.get("SELECT * FROM Integrante WHERE id = ? ", [idIntegrante], (err, integrante) => {
            if (err) {
                console.error("Error al obtener el integrante:", err);
                return res.status(500).render("error");
            }

            if (!integrante) {
                return res.redirect('/admin/Integrante/listar?message=No se encontró el integrante');
            }

            res.render("admin/Integrante/editForm", { integrante });
        });
    },
    destroy(req, res) {
        const idIntegrante = req.params.idIntegrante;

        db.run("UPDATE Integrante SET esta_borrado = 1 WHERE id = ?", [idIntegrante], function (err) {
            if (err) {
                console.error("Error al marcar el integrante como inactivo:", err);
                return res.redirect('/admin/Integrante/listar?message=Error al eliminar el integrante');
            }

            console.log(`Integrante con ID ${idIntegrante} marcado como inactivo.`);
            res.redirect('/admin/Integrante/listar?message=Integrante eliminado exitosamente');
        });
    }
}

module.exports = IntegranteController;
